#!/bin/bash

printf "Create user directories"

{
  mkdir -p $HOME/.local/bin/
  mkdir -p $HOME/.config/awesome/
  mkdir $HOME/Pictures/
  mkdir $HOME/Downloads/
  mkdir $HOME/Music/
  mkdir $HOME/Desktop/
  mkdir $HOME/Videos/
  mkdir -p $HOME/Documents/git_repositories/
} > /dev/null

printf "Delete unused windows managers"
printf "\n"

{
  sudo dnf remove -y dwm* i3* *ratpoison* *qtile* *xmonad* *openbox*
} > /dev/null

printf "Install RPM Fusion"


sudo dnf install -y \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 


printf "Install packages"

{
  sudo dnf -y install awesome \
    git \
    compton \
    make \
    libX11-devel \
    libXt-devel \
    libXft-devel \
    dmenu \
    libXfont2-devel \
    light \
    zathura-plugins-all \
    feh \
    gcc \
    i3lock \
    tmux \
    acpi \
    xclip \
    gtk3-devel \
    neovim \
    firefox \
    htop \
    mpv \
    youtube-dl \
    neofetch \
    nnn \
    ImageMagick \
    cmus \
    newsboat \
    pandoc \
    neovim \
    NetworkManager-tui \
    pavucontrol \
    pcmanfm \
    scrot \
    levien-inconsolata-fonts \
    mozilla-fira-mono-fonts \
    mozilla-fira-sans-fonts \
    mozilla-fira-fonts-common \
    terminus-fonts \
    --skip-broken

  echo "set selection-clipboard clipboard" >> .config/zathura/zathurarc
} > /dev/null

printf "Install Suckless Terminal (st)"

{
  sudo dnf -y copr enable jeremieglbd/st \
    && sudo dnf install -y st-0.8.2-1
} > /dev/null

printf "Clone .Xressources repository"

{
git clone https://gitlab.com/jeremieglbd/xressources-config.git \
	$HOME/Documents/git_repositories/xressources-config
ln $HOME/Documents/git_repositories/xressources-config/.Xressources \
	$HOME/
xrdb $HOME/.Xressources
} > /dev/null

printf "Clone Awesome config files repository"

{
git clone https://gitlab.com/jeremieglbd/awesome-config-files.git \
	$HOME/Documents/git_repositories/awesome-config-files
ln $HOME/Documents/git_repositories/awesome-config-files/rc.lua \
	$HOME/.config/awesome
ln $HOME/Documents/git_repositories/awesome-config-files/autorun.sh \
        $HOME/.config/awesome
ln -s $HOME/Documents/git_repositories/awesome-config-files/widgets \
        $HOME/.config/awesome
} > /dev/null

printf "Install xdragon"

{
  sudo dnf -y copr enable jeremieglbd/xdragon \
    && sudo dnf install -y xdragon
  } > /dev/null

printf "Clone tutorials repository"

{
  git clone https://gitlab.com/jeremieglbd/tutoriels-divers.git \
    $HOME/Documents/git_repositories/tutoriels-divers
  } > /dev/null

printf "Install R? [y/n]: "
read -p -n 1 installR
[ "$installR" = "y" -o "$installR" = "o" ] \
  && sudo dnf install R \
  || printf "R won't be available"

printf "\n"

printf "Install TexLive ? [y/n]:"
read -p -n 1 installLaTeX
[ "$installLaTeX" = "y" -o "$installLaTeX" = "o" ] \
  && rm -rf $HOME/Downloads/install-tl* \
  && sudo rm -rf /usr/local/texlive \
  && wget -P $HOME/Downloads \
      http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz \
  && tar -xzvf $HOME/Downloads/install-tl*.tar.gz -C Downloads/
  && rm -f $HOME/Downloads/install-tl*.tar.gz
  && chmod +x $HOME/Downloads/install-tl*/install-tl
  && sudo mkdir /usr/local/texlive 
  && sudo chown $USER /usr/local/texlive \
  && $HOME/Downloads/install-tl*/install-tl \
  && LaTeXFolder="$(ls /usr/local/texlive/ | grep -E '[0-9]{1,4}')" \
  && echo 'export PATH=/usr/local/texlive/$LaTeXFolder/bin/x86_64-linux:$PATH' >> \
    $HOME/.bashrc \
  || printf "Texlive won't be available"

printf "\n"

printf "Install and use Flatpak for applications? [y/n]: "
read -p -n 1 useFlatpak

if [ "$useFlatpak" = "y" -o "$useFlatpak" = "o" ] ;; then
  sudo dnf -y install flatpak
  flatpak remote-add --if-not-exists \
    flathub https://flathub.org/repo/flathub.flatpakrepo

  printf "\n"

  printf "Install LibreOffice? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub org.libreoffice.LibreOffice

  printf "\n"

  printf "Install Gimp? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub org.gimp.GIMP

  printf "\n"

  printf "Install Telegram? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub org.telegram.desktop

  printf "\n"

  printf "Install Wire? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub com.wire.WireDesktop

  printf "\n"

  printf "Install Nextcloud client? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    flatpak install flathub org.nextcloud.Nextcloud

  printf "\n"

  printf "Install KeePassXC? [y/n]:"
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub org.keepassxc.KeePassXC

  printf "\n"

  printf "Install Steam? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub com.valvesoftware.Steam

  printf "\n"

  printf "Install qBittorrent? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && flatpak install flathub org.qbittorrent.qBittorrent

  printf "\n"

else
  printf "\n"

  printf "Install LibreOffice? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y libreoffice libreoffice-langpack-fr

  printf "\n"

  printf "Install Gimp? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y gimp

  printf "\n"

  printf "Install Telegram? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y telegram-desktop

  printf "\n"

  printf "Install Nextcloud client? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y nextcloud-client

  printf "\n"

  printf "Install Steam ? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y steam

  printf "\n"

  printf "Install KeePassXC? [y/n]: "
  read -p -n 1 answer
  if [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y keepassxc

  printf "\n"

  printf "Install qbittorrent? [y/n]: "
  read -p -n 1 answer
  [ "$answer" =  "y" -o "$answer" = "o" ] \
    && sudo dnf install -y qbittorrent

  printf "\n"

fi

