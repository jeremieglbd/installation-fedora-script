Bash script for Fedora installation
---

Uses my gitlab repositories and copr projects for several applications
and configuration files.
It is possible to choose between flatpak or rpm installation for applications.
Automation of TexLive installation.

Tested with [netinstall](https://getfedora.org/en/server/download/) iso
and with `minimal desktop` option.
